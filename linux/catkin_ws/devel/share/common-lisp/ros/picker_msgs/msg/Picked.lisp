; Auto-generated. Do not edit!


(cl:in-package picker_msgs-msg)


;//! \htmlinclude Picked.msg.html

(cl:defclass <Picked> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (selected
    :reader selected
    :initarg :selected
    :type cl:string
    :initform "")
   (goal
    :reader goal
    :initarg :goal
    :type geometry_msgs-msg:Vector3
    :initform (cl:make-instance 'geometry_msgs-msg:Vector3)))
)

(cl:defclass Picked (<Picked>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Picked>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Picked)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name picker_msgs-msg:<Picked> is deprecated: use picker_msgs-msg:Picked instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <Picked>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader picker_msgs-msg:header-val is deprecated.  Use picker_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'selected-val :lambda-list '(m))
(cl:defmethod selected-val ((m <Picked>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader picker_msgs-msg:selected-val is deprecated.  Use picker_msgs-msg:selected instead.")
  (selected m))

(cl:ensure-generic-function 'goal-val :lambda-list '(m))
(cl:defmethod goal-val ((m <Picked>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader picker_msgs-msg:goal-val is deprecated.  Use picker_msgs-msg:goal instead.")
  (goal m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Picked>) ostream)
  "Serializes a message object of type '<Picked>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'selected))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'selected))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'goal) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Picked>) istream)
  "Deserializes a message object of type '<Picked>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'selected) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'selected) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'goal) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Picked>)))
  "Returns string type for a message object of type '<Picked>"
  "picker_msgs/Picked")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Picked)))
  "Returns string type for a message object of type 'Picked"
  "picker_msgs/Picked")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Picked>)))
  "Returns md5sum for a message object of type '<Picked>"
  "db629bff49e5adf5a285d464babb8ebe")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Picked)))
  "Returns md5sum for a message object of type 'Picked"
  "db629bff49e5adf5a285d464babb8ebe")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Picked>)))
  "Returns full string definition for message of type '<Picked>"
  (cl:format cl:nil "Header header~%~%string selected~%~%geometry_msgs/Vector3 goal~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Picked)))
  "Returns full string definition for message of type 'Picked"
  (cl:format cl:nil "Header header~%~%string selected~%~%geometry_msgs/Vector3 goal~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Picked>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:length (cl:slot-value msg 'selected))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'goal))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Picked>))
  "Converts a ROS message object to a list"
  (cl:list 'Picked
    (cl:cons ':header (header msg))
    (cl:cons ':selected (selected msg))
    (cl:cons ':goal (goal msg))
))
