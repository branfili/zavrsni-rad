
(cl:in-package :asdf)

(defsystem "picker_msgs-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :geometry_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "Picked" :depends-on ("_package_Picked"))
    (:file "_package_Picked" :depends-on ("_package"))
  ))