;; Auto-generated. Do not edit!


(when (boundp 'picker_msgs::Picked)
  (if (not (find-package "PICKER_MSGS"))
    (make-package "PICKER_MSGS"))
  (shadow 'Picked (find-package "PICKER_MSGS")))
(unless (find-package "PICKER_MSGS::PICKED")
  (make-package "PICKER_MSGS::PICKED"))

(in-package "ROS")
;;//! \htmlinclude Picked.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass picker_msgs::Picked
  :super ros::object
  :slots (_header _selected _goal ))

(defmethod picker_msgs::Picked
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:selected __selected) "")
    ((:goal __goal) (instance geometry_msgs::Vector3 :init))
    )
   (send-super :init)
   (setq _header __header)
   (setq _selected (string __selected))
   (setq _goal __goal)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:selected
   (&optional __selected)
   (if __selected (setq _selected __selected)) _selected)
  (:goal
   (&rest __goal)
   (if (keywordp (car __goal))
       (send* _goal __goal)
     (progn
       (if __goal (setq _goal (car __goal)))
       _goal)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; string _selected
    4 (length _selected)
    ;; geometry_msgs/Vector3 _goal
    (send _goal :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; string _selected
       (write-long (length _selected) s) (princ _selected s)
     ;; geometry_msgs/Vector3 _goal
       (send _goal :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; string _selected
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _selected (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; geometry_msgs/Vector3 _goal
     (send _goal :deserialize buf ptr-) (incf ptr- (send _goal :serialization-length))
   ;;
   self)
  )

(setf (get picker_msgs::Picked :md5sum-) "db629bff49e5adf5a285d464babb8ebe")
(setf (get picker_msgs::Picked :datatype-) "picker_msgs/Picked")
(setf (get picker_msgs::Picked :definition-)
      "Header header

string selected

geometry_msgs/Vector3 goal

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
")



(provide :picker_msgs/Picked "db629bff49e5adf5a285d464babb8ebe")


