import rospy

from picker_msgs.msg import Picked
from geometry_msgs.msg import Vector3

def PickedSub():
    rospy.init_node('PickedSub', anonymous=True)

    picked_subscriber = rospy.Subscriber("picked", Picked, handlePicked, queue_size=10)

    rospy.spin()

def handlePicked():

if __name__ == '__main__':
    PickedSub()
