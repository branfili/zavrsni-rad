# CMake generated Testfile for 
# Source directory: /home/branfili/Desktop/catkin_ws/src
# Build directory: /home/branfili/Desktop/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(robot_spawner)
subdirs(file_server)
subdirs(picker_msgs)
subdirs(turtlebot_scene)
