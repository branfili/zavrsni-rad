\documentclass[times, utf8, zavrsni]{fer}

\usepackage{booktabs}
\usepackage{listings}
\usepackage{pdfpages}

\begin{document}

\thesisnumber{5596}

\title{Upravljanje mobilnim robotima pomoću holograma naočala za proširenu stvarnost}

\author{Branimir Filipović}

\maketitle

\newpage
\includepdf[pages=-]{izvornik/zadatak.pdf}

\zahvala{Mojoj obitelji, mentoru i komentoru, koji su me podržavali u napetim trenutcima pisanja ovog rada.}

\tableofcontents

\listoffigures

\chapter{Uvod}

U zadnje vrijeme svi smo bombardirani novim \emph{cool} riječima i pojmovima.
Nekad su to bili \glqq Internet 2.0 \grqq i \glqq Big Data \grqq, 
a danas su to proširena \engl{AR} i virtualna stvarnost \engl{VR}.

Proširena stvarnost je poboljšana verzija fizikalnog, realnog svijeta u kojoj 
su neki elementi generirani pomoću računala ili izvučenih stvarnih osjetilnih 
podražaja, kao npr. auditorni ili haptički podražaji. \citep{bok:compendium}

Baš zato je proširena stvarnost brzo postala jedna od najpopularnijih grana računarstva, 
jer ima mnogo mogućih primjena, baš kao što su pametni telefoni \engl{smartphone} bili 
u prošlom desetljeću.

U ovom radu istražujemo jednu od primjena proširene stvarnosti u području robotike, 
u svrhu lakše vizualizacije kretanja robota u prostoru, te eventualne sinkronizacije 
virtualnih i pravih robota.

\chapter{Korištene tehnologije}

\section{Stražnja strana \engl{back end}}

\subsection{ROS}

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.3]{slike/rosLogo.jpg}
  \caption{Logo ROS-a}
  \label{fig:ros-logo}
\end{figure}

ROS (Robotski Operacijski sustav) je skup knjižnica \engl{library} 
i alata \engl{tool} koji pomažu u pisanju aplikacija za robote.

Aplikacije se sastoje od čvorova \engl{node}, koji međusobno 
komuniciraju prenoseći poruke \engl{message}. 
Poruke se prenose preko tema \engl{topic} kao npr. \emph{joint\_states}.
Čvorovi mogu biti primatelji \engl{subscriber} ili pošiljatelji \engl{publisher} poruka. 
\citep{www:roswiki}

\lstset{language=XML, 
        captionpos=b,
        showstringspaces=false, 
        caption=Primjer Launch datoteke,
        label=code:robot}
\lstinputlisting{kodovi/robot.launch}

Kod \ref{code:robot} prikazuje jednu jednostavnu Launch datoteku.
\footnote{Launch datoteke su podvrsta XML datoteka koje opisuju kako se neki
čvor treba pokretati}
Ta datoteka opisuje stvaranje jednog čvora pomoću kojeg se u Gazebo aplikaciji 
stvara jedan robot s inicijalnim \engl{default} početnim koordinatama \((0, 0, 0)\) 
i Eulerovim kutevima \footnote{Eulerovi kutevi su način zapisa 3D rotacija posebno 
po svakoj osi. \citep{www:eulermathworld}}
\((0, 0, 0)\).
Te početne vrijednosti moguće je promijeniti kroz argumente datoteke.

Također, datoteci je potrebno predati naziv robota, te put do URDF datoteke
\footnote{URDF datoteke su podvrsta XML datoteka u kojoj je opisana struktura robota 
\citep{www:roswiki}}.

Prikazani kod koristim prilikom svakog stvaranja modela robota u Gazebo aplikaciji.

\subsection{Gazebo}

Gazebo je besplatni, otvoreni \engl{open-source} simulator robotike pisan za operacijski sustav
Linux Ubuntu koji omogućava jednostavnu simulaciju različitih svjetova i robota.
Gazebo potpuno podržava ROS.

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.3]{slike/gazeboLogo.png}
  \caption{Logo Gazebo-a}
  \label{fig:gazebo-logo}
\end{figure}

\newpage

\section{Prednja strana \engl{front end}}

\subsection{Unity}

Unity je višeplatformska jezgra igara \engl{game engine} dizajnirana za izradu 2D 
i 3D video igara.

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.5]{slike/unityLogo.png}
  \caption{Logo Unity-a}
  \label{fig:unity-logo}
\end{figure}

Prva verzija puštena je u promet 2008.\ godine.
Pisana je u C++-u i C\#-u.

\subsubsection{Mogućnosti Unity-a}

Neke od platformi koje Unity podržava su\footnote{\citep{www:unity-platforms}}:
\begin{itemize}
  \item Windows
  \item Linux
  \item Android
  \item iOS
  \item Playstation 4
  \item Xbox One
  \item Nintendo Switch
  \item Windows Mixed Reality (Microsoft HoloLens)
\end{itemize}

\newpage

Kao što vidimo na slikama \ref{fig:ksp-unity} i \ref{fig:subnautica-unity}, Unity 
može napraviti stvarno vizualno impresivne aplikacije.\footnote{\citep{www:unity-games}}

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.5]{slike/kspUnity.png}
  \caption{Kerbal Space Program}
  \label{fig:ksp-unity}
\end{figure}

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.5]{slike/subnauticaUnity.jpg}
  \caption{Subnautica}
  \label{fig:subnautica-unity}
\end{figure}

\newpage

\subsubsection{Rad u Unity-u}

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.4]{slike/unityGUI.png}
  \caption{Prikaz korisničkog sučelja Unity-a 2017.4.3f1}
  \label{fig:unitygui}
\end{figure}

Slika \ref{fig:unitygui} prikazuje korisničko sučelje Unity dijela aplikacije.
Ovu aplikaciju radio sam u Unity verziji 2017.4, preporučenoj verziji za rad na 
Microsoft Hololens sustavu.

Korisničko sučelje bi podijelio na nekoliko bitnih dijelova\footnote{\citep{www:unity-manual}}:
\begin{description}
  \item[Alatna traka scene] - prikazana u gornjem lijevom kutu, ispod alatne trake
    Unity-a.
    
    Sastoji se od nekoliko bitnih akcija na predmetima \engl{GameObject}:
        \begin{itemize}
          \item Odabir
          \item Translacija
          \item Rotacija
          \item Skaliranje
          \item Pravokutna transformacija
          \item Transformacija
        \end{itemize}

        Tu se također nalaze i gumbi za pokretanje, pauziranje i zaustavljanje aplikacije.
\newpage

  \item[Prozor scene] - lijevi gornji prozor. Tu se nalazi interaktivna scena te se
    u tom prozoru radi namještanje predmeta u sceni pomoću prethodno opisanih akcija.
  
  \item[Prozor aplikacije] - lijevi donji prozor. Nakon pokretanja aplikacije, tu se 
    prikazuje njezin rad, te su dopuštene sve radnje kao i u konačnoj aplikaciji.
  
  \item[Prozor grafa scene] - srednje lijevi prozor. Tu su popisani svi predmeti u sceni, 
    kao i njihov međusobni odnos (npr. predmet \emph{ImageReceiver} je dijete od predmeta 
    \emph{Scene})

  \item[Prozor datoteka] - srednje desni prozor. Tu su popisane sve datoteke koje se koriste 
    u izradi aplikacije. 
    
    Najčešći tipovi datoteka su:
      \begin{itemize}
        \item Materijali
        \item Predlošci \engl{Prefab}
        \item C\# skripte
        \item Animacije
        \item Zvukovi
      \end{itemize}

  \item[Prozor predmeta] - desni prozor. Ako je odabran neki predmet u sceni, tu se prikazuje 
    popis svih svojstava \engl{Component} tog predmeta. 
    Nova svojstva se mogu dodati i direktno iz prozora datoteka.
    Također je moguće \glqq ručno \grqq namještati sve parametre.

    Najčešći tipovi svojstava su:
      \begin{itemize}
        \item Transformacija
        \item Iscrtavatelj \engl{Renderer}
        \item Detektor sudara \engl{Collider}
        \item C\# skripta
        \item Animacija
        \item Zvuk
      \end{itemize}

\end{description}

\newpage

\subsection{Microsoft HoloLens}
\label{chap:glasses}

Microsoft Hololens su naočale za prošireni vid koje je razvio Microsoft.
Razvojna verzija naočala puštena je u slobodni promet 2016. godine.

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.3]{slike/hololensGlasses.jpeg}
  \caption{Microsoft Hololens naočale}
  \label{fig:hololens-glasses}
\end{figure}

Razvoj aplikacija za Microsoft HoloLens se radi pomoću Unity jezgre igara, te Microsoft 
Visual Studija, radne okoline prilagođene pisanju koda u C++-u i C\#-u.

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.15]{slike/visualStudioLogo.png}
  \caption{Logo Microsoft Visual Studija}
  \label{fig:visual-studio-logo}
\end{figure}

Kako bi se mogle razvijati holografske aplikacije, HoloLens nudi
nekoliko osnovnih načina interakcije s korisnikom i svijetom\footnote{\citep{www:hololens2018}}:
\begin{itemize}
  \item Praćenje pogleda \engl{gaze}
  \item \glqq Klikanje \grqq \engl{air tap, gesture}
  \item Prepoznavanje glasa
  \item Koordinate u svijetu
  \item Mapiranje zvuka u prostor
  \item Mapiranje prostora
\end{itemize}

Kao što je vidljivo iz priloženog, Microsoft Hololens naočale nude puno 
funkcionalnosti korisne razvijateljima raznih aplikacija \engl{software developers}.

\newpage

\subsubsection{Praćenje pogleda}

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.5]{slike/hololensGaze.png}
  \caption{Način praćenja pogleda Hololens sustavom}
  \label{fig:hololens-gaze}
\end{figure}

Praćenje pogleda jedno je od osnovnih načina interakcije s korisnikom. 
Pogledom korisnik izražava svoju namjeru. \citep{www:hololens-gaze}

Praćenje pogleda se radi uz pomoć bacanja zrake \engl{ray casting} od korisnikove 
glave prema sceni, te traženje objekta u sceni s kojim se zraka sječe.

\subsubsection{\glqq Klikanje \grqq}

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.4]{slike/hololensGesture0.png}
  \includegraphics[scale=0.4]{slike/hololensGesture1.png}
  \caption{Način \glqq klikanja \grqq u Hololens sustavu}
  \label{fig:hololens-gesture}
\end{figure}

\glqq Klikanje \grqq je također jedna od osnovnih načina interakcije s korisnikom. \\
\glqq Klikanjem \grqq korisnik potvrđuje svoj odabir koji je načinio pomoću 
praćenja pogleda. \citep{www:hololens-gesture}

\subsubsection{Primjena u aplikaciji}

U aplikaciji su neke od tih funkcionalnosti iskorištene na sljedeći način:
\begin{description}
  \item [Praćenje pogleda:] Koristi se za odabir objekata u sceni. \\ 
          Korisnik može odabrati robote ili točku na podu do koje će se robot kretati.
  \item [\glqq Klikanje: \grqq] Koristi se za pritisak gumba. \\
          Postoji 5 gumba u sceni, čija funkcionalnost je detaljnije 
          opisana u poglavlju \ref{chap:dokumentacija}.
\end{description}

\newpage

\section{Povezanost}

\subsection{ROS\#}

ROS\# je skup biblioteka i alata koji olakšava komunikaciju ROS-a sa .NET aplikacijama, 
u prvom redu Unity-em.

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.05]{slike/rosSharpLogo.png}
  \caption{Logo ROS\#-a}
  \label{fig:rossharplogo}
\end{figure}

Pomoću ROS\# možemo\footnote{\citep{www:rossharp2018}}:
\begin{itemize}
  \item učitavati URDF modele robota kao GameObject klasu u Unity
  \item kontrolirati robota pomoću Unity-a, s vezom prema ROS-u / Gazebo-u
  \item simulirati robota u Unity-u, bez veze prema ROS-u / Gazebo-u
\end{itemize}

Kao što je vidljivo iz priloženog, ROS\# je bio integralni dio 
ove aplikacije, o čemu ću detaljnije u poglavlju \ref{chap:tutorial}.

\newpage

\chapter{Izrada aplikacije}

\section{Upoznavanje s alatom ROS\#}
\label{chap:tutorial}

U prvom dijelu pisanja aplikacije prošao sam vodič \engl{tutorial} koji je napisan na 
wiki-stranici alata ROS\#\footnote{\citep{www:rossharpwiki}}.

Vodič se sastoji od sljedećih poglavlja:
\begin{enumerate}
  \item Instalacija Unity-a na Windows 10 operacijski sustav
  \item Instalacija Linux Ubuntu operacijskog sustava na Oracle virtualni
    stroj \engl{VM}
  \item Postavljanje ROS stražnje strane na Ubuntu
  \item Postavljanje Gazebo stražnje strane na Ubuntu
  \item Instalacija podrške za Turtlebot robota
  \item Učitavanje URDF datoteke iz ROS-a u Unity (kao predmet)
  \item Pokretanje aplikacije
\end{enumerate}

\newpage

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.2]{slike/rosSharpTutorial.png}
  \caption{Prikaz aplikacije nakon prijeđenog vodiča}
  \label{fig:rosSharpTutorial}
\end{figure}

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.35]{slike/tutorialGraph.png}
  \caption{Graf ROS čvorova nakon prijeđenog vodiča}
  \label{fig:tutorialGraph}
\end{figure}

Nakon prijeđenog vodiča, imao sam spremnu jednu jednostavnu aplikaciju, koja
je prikazana na slici \ref{fig:rosSharpTutorial}.
ROS arhitektura prikazana je na slici \ref{fig:tutorialGraph}.

Aplikacija ima glavnu petlju te redom:
\begin{enumerate}
  \item Sluša kontrole (npr. tipkovnicu ili joystick) u Unity-u
  \item Šalje te informacije Gazebo-u
  \item Simulira kretanje robota u Gazebo-u
  \item Šalje nazad pokrete koje Unity mora vizualizirati.
\end{enumerate}
    
Također, Unity prima i video prikaz iz pozicije robota, koji se prikazuje korisniku.

\newpage

\section{Izrada Unity aplikacije}

S jednom jednostavnom aplikacijom kao bazom, počeo sam raditi na prilagođavanju 
postojeće aplikacije mojim potrebama.

Dodao sam još jednog Turtlebota pomoću prethodno spomenutog vodiča te sam započeo s modifikacijama kako bi mogao:
\begin{enumerate}
  \item odabrati kojeg robota kontroliram
  \item odabrati mjesto do kojeg će se robot 
        eventualno navigirati
\end{enumerate}

Kako bih to sve ostvario, dodao sam svoj novi tip poruke, \emph{picker\_msgs/Picked.msg}.
Ta poruka odašilje se preko teme \emph{/picked}, te je prikazana u kodu \ref{code:pickedMsg}.

\lstset{showstringspaces=false, 
        captionpos=b,
        caption=Primjer datoteke poruke,
        label=code:pickedMsg}
\lstinputlisting{kodovi/Picked.msg}

Također sam oba robota odvojio u vlastite prostore imena \engl{namespace}, kako ne bi došlo do kolizije informacija.
Prostor imena 1.\ robota je \emph{/robot000}, a 2.\ robota \emph{/robot001}.

Na kraju, sve što je bilo potrebno je promijeniti sve teme tako da započinju s odgovarajućim prefiksom.
Također, bilo je potrebno napisati skripte kako bi se prilikom odabira robota taj prefiks mijenjao.

\newpage

Na stražnjoj strani, modificirao sam postojeću Python skriptu \emph{joy\_to\_twist.py} koja pretvara 
ulazne signale u upute za kretanje robota. Modificirane dijelove skripte prikazani su 
u kodu \ref{code:joyToTwist}.

\lstset{language=Python,
        captionpos=b,
        showstringspaces=false, 
        caption=Primjer skripte na stražnjoj strani,
        label=code:joyToTwist}
\lstinputlisting[firstline=32,
                 lastline=47]
                 {kodovi/joy_to_twist.py}

Na prednjoj strani dodao sam puno novih C\# skripti koje slušaju odabir korisnika, 
te šalju te poruke stražnjoj strani.

Jedna od takvih skripti je \emph{PickedListener.cs}, razred koji sluša događanja na sceni, 
te određuje što je korisnik odabrao (pod ili robota). Kod \ref{code:PickedListener} je prikazuje 
u cijelosti.

\lstset{language=csh,
        captionpos=b,
        showstringspaces=false, 
        caption=Primjer skripte na prednjoj strani,
        label=code:PickedListener}
\lstinputlisting{kodovi/PickedListener.cs}

\newpage

Nakon što je sve bilo gotovo, aplikacija je izgledala kao što je prikazano na slikama 
\ref{fig:unity-scene} i \ref{fig:gazebo-scene}.
Nova ROS arhitektura prikazana je na slici \ref{fig:finalGraph}.

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.17]{slike/unityTurtlebotScene.png}
  \caption{Prikaz virtualne scene u Unity PC aplikaciji}
  \label{fig:unity-scene}
\end{figure}

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.12]{slike/gazeboTurtlebotScene.png}
  \caption{Prikaz virtualne scene u Gazebo aplikaciji}
  \label{fig:gazebo-scene}
\end{figure}

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.2]{slike/finalGraph.png}
  \caption{Konačni graf ROS čvorova}
  \label{fig:finalGraph}
\end{figure}

\newpage

\section{Selidba aplikacije na HoloLens naočale}

Nakon završene Unity aplikacije za PC, započeo sam sa seljenjem
aplikacije na Hololens sustav.

Najprije sam skinuo Mixed Reality Toolkit\footnote{\citep{www:holotoolkitwiki}} od Microsofta, 
koji nudi razne funkcionalnosti vezane uz Hololens korisničko sučelje.

Zatim sam promijenio trenutno korisničko sučelje tako da bolje odgovara mogućnostima 
Hololens naočala.

Novo korisničko sučelje prikazano je na slici \ref{fig:hololens-scene} te je opisano u 
poglavlju \ref{chap:dokumentacija}.

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.3]{slike/hololensTurtlebotScene.png}
  \caption{Prikaz virtualne scene u Unity Hololens aplikaciji}
  \label{fig:hololens-scene}
\end{figure}

\newpage

Jedna od novih C\# skripti koje sam napisao prikazana je u kodu \ref{code:ButtonSelector}.
Ona se brine o tome da se prethodno odabrani robot pritiskom virtualnih gumbi pomiče u 
pravom smjeru.

\lstset{language=csh,
        captionpos=b,
        showstringspaces=false,
        caption=Primjer skripte za novo korisničko sučelje,
        label=code:ButtonSelector}
\lstinputlisting{kodovi/ButtonSelector.cs}

\newpage

\subsection{Problemi}

Nažalost, alat ROS\# je napisan sa zavisnostima \engl{dependency} koje su pisane za .NET verziju 3.5.
Međutim, sve Hololens aplikacije trebaju podržavati najmanje .NET verziju 4.6.

Stoga sam morao isključiti zavisnost \emph{websocket-sharp.dll} i naći neku kompatibilnu implementaciju 
WebSocket protokola\footnote{WebSocket protokol omogućava dvosmjernu komunikaciju između klijenta koji izvodi 
nepouzdani kod i servera koji odlučuje vjerovati tom klijentu \citep{www:websocket}}.

To sam riješio tako da sam umjesto \emph{Websocket.cs} datoteke, uključio \emph{UWPWebsocket.cs} datoteku u 
tu \emph{DLL} datoteku. \citep{www:uwp-websocket} 

Zgodno je za primijetiti da tim programera koji razvijaju ROS\# rade na istom tom problemu, 
istovremeno s pisanjem ovog Rada. \citep{www:rossharp-issue}

\newpage

\chapter{Instalacija i korištenje aplikacije}
\label{chap:dokumentacija}

Prilikom instalacije potrebno je skinuti Unity packet \emph{HololensTurtlebot.unitypackage}, učitati ga u 
Unity, izgraditi \engl{build} aplikaciju za Hololens naočale te ih pomoću Visual Studija staviti \engl{deploy} 
na naočale.

Po želji, nakon učitavanja paketa, moguće je učitati druge robote odabirom \\
\emph{RosBridgeClient / Import URDF Assets ...} opcije u alatnoj traci, te praćenjem uputa 
koje su došle uz potporu za stražnju stranu.

Zatim, potrebno je na drugom stroju sa Ubuntu Linux operacijskim sustavom instalirati odgovarajuće \emph{catkin} 
pakete pomoću već spomenutih uputa.

Na kraju, potrebno je pokrenuti aplikaciju pomoću konzole \engl{Terminal}, 
upisom sljedeće naredbe:\\
\begin{center}
  \emph{roslaunch turtlebot\_scene turtlebot\_scene.launch}
\end{center}

U virtualnoj sceni koja je prikazana na slici \ref{fig:hololens-scene} jasno se vide 5 gumba i jedno platno.
Na platnu se prikazuje slika koju vidi trenutno odabrani robot.
Središnji gumb odabire robota u kojeg korisnik trenutno gleda, ili zabilježi poziciju na podu prema kojem korisnik gleda.
Ostala 4 gumba pomiču trenutno odabranog robota u jednom od 4 glavna smjera:
\begin{itemize}
  \item gore
  \item desno
  \item dolje
  \item lijevo
\end{itemize}

Ostvarivanje opisanih elemenata sučelja detaljnije je bilo 
objašnjeno u poglavlju \ref{chap:glasses}.

\chapter{Zaključak}

Ova aplikacija dobro pokazuje mogućnosti proširene stvarnosti.
Na jednostavan i vizualno impresivan način, pokazuje kako bi 
se roboti kretali, bez potrebe za skupim isprobavanjem sa stvarnim robotima.

Nažalost, u ovu aplikaciju nisam uspio ugraditi mogućnost da se roboti 
sami navigiraju do proizvoljno odabrane točke, ali to bi svakako napravio 
u budućnosti.

Jednu od primjena ove aplikacije bi mogao vidjeti u dizajnu robota, prije nego 
što se robot uistinu proizvede. Na taj način bi se jednostavno moglo vidjeti očekivano 
ponašanje robota, i tako raditi modifikacije dizajna prije proizvodnje.

Zbog ove aplikacije, a i mnogih drugih, možemo sa uzbuđenjem iščekivati što nam 
budućnost proširene stvarnosti donosi.

\bibliography{literatura}
\bibliographystyle{fer}

\begin{sazetak}

Proširena stvarnost jedna je od najnovijih tehnologija koja se pojavila na tržištu. 
U zoru svake nove tehnologije, pa tako i proširene stvarnosti, uvijek istražujemo 
razne primjene, pa tako i ova aplikacija.

Aplikacija se sastoji od dva dijela: vizualizacije, koja se vrti na Microsoft Hololens 
naočalama; i simulacije, koja se vrti u Gazebo aplikaciji na povezanom serveru.

Unutar aplikacije moguće je odabirati i pomicati robote, i tako na jednostavan način vizualizirati 
ponašanje robota.

Jedna od primjena ove aplikacije moguća je u dizajnu robota, gdje je na jednostavan način moguće 
isprobavati dizajn prije proizvodnje, te ga po potrebi modificirati.

\kljucnerijeci{ROS, Gazebo, Unity, Microsoft Hololens, vizualizacija}
\end{sazetak}

\newpage

\engtitle{Mobile robot control using augmented reality holograms}
\begin{abstract}

Augmented reality (AR) is one of the newest technologies to hit the market. 
In the dawn of every new technology, and so AR too, we always explore different 
applications, and so this app too.

App consists of two parts: visualization, that runs on Microsoft Hololens glasses; 
and simulation, that runs on Gazebo app on a connected server.

Inside the app, it's possible to select and move robots, and so in a simple way visualize 
the behaviour of robots.

One of the applications of this app is possible in robot design, where in a simple way 
it's possible to try out the design before manufacturing, and to modify it if needed.

\keywords{ROS, Gazebo, Unity, Microsoft Hololens, visualization}
\end{abstract}

\end{document}
