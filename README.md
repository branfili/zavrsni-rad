# Installation and usage of Hololens/ROS project HoloUpravljanje

## Technology stack setup

Firstly, you'll need a __Windows 10 OS__ with a __Oracle VM VirtualBox__ installed.
__VirtualBox Installer__ can be downloaded [here][VM].

Next, you'll have to install the __64-bit Ubuntu__ Virtual Machine on the __VirtualBox__.
More information about that can be found [here][Ubuntu].

Following that you'll have to install __Microsoft Visual Studio__, more information 
can be found [here][VS].

Finally, you'll also have to install __Unity Game Engine__, for which the installer can be found [here][Unity].
Please keep in mind that this app was written using **version _2017.4.3_**, so that version is
recommended, for now.
Also, you'll need the __.NET scripting backend__.

### Ubuntu Setup

First, you have to setup the **_Network Adapters_**, because Gazebo application must be able to 
communicate with the Hololens application. 
That can be done under __Settings > Network__.
You'll need two Network Adapters
1. NAT Adapter
2. Bridged Wireless Network Adapter

Next, turn on the VM and run the [installation script](./install.sh), to setup the _Gazebo, ROS and Turtlebots_.
Remeber that the installation script will ask for your admin password (it uses the command 
__sudo__).

Following that, you'll have to replace some directories.
* Replace the turtlebot_gazebo and rosbridge_server directories 
on the path */opt/ros/kinetic/share*
* Replace the catkin_ws directory on the path *~/Desktop*

Finally, run the command on the path *~/Desktop/catkin_ws*
```bash
sudo catkin build
```

### Unity Setup

Just import the __HoloUpravljanje Unity Package__ into a new Unity project.

## Deploying the app

Before deploying, check the private IP address of the VM.
That can be done by checking the _inet4 address_ of the _enp0s8_ adapter after runing `ifconfig`
(it should be in the form of 192.168.x.x).

### Unity app

Go to the Unity Project, and click the Game Object __RosConnector__.
Now change the value of the field _Ros Bridge Server URI_ of the Component **_Ros Connector (Script)_** 
into the IP address of the VM.
Keep the port 9090.

Now, build the UWP project by typing __Ctrl+Shift+B__ and then choosing the option _Build_.
Remember to build in a new directory, because it will create a __new Visual Studio Solution__.
Make sure that you __have the following options__:
* You have switched platform to __Universal Windows Platform__
* Target Device: Hololens
* Build Type: D3D
* Build and Run on: Local Machine
* All others: Latest Installed
* When going to __Player Settings > Other Settings__
  * Scripting Backend: .NET (must be installed)
  * API Compatibility Level: .NET 4.6

Next, build the new Visual Studio Solution.

After building, deploy in __Debug x86 Mode on a Wireless Machine__.
Just input the private IP address of the Hololens, and that's it.
__Make sure that the Hololens is on a same wireless network as the Windows 10!__
To find out the IP address of the Hololens, go to __Settings > Network & Internet > 
WiFi > Advanced Options (of the current network)__. 

__Remeber to start the Gazebo app before starting the Hololens app!__

### Gazebo app

Just run the following command:
```bash
roslaunch turtlebot_gazebo turtlebot_gazebo.launch
```

## Using the app

Currently, there is no much use, but soon you'll be able to move the Turtlebots using gestures 
on the floor.

## Developer notes

### Adding own message types

* Unzip *.\windows\lib\RosBridgeClient.zip*
* Open the VS Project
* Change source files _MessageTypes.cs_ and _Messages.cs_
* Build the project
* Replace file *$PATH_TO_UNITY_PROJECT\Assets\RosSharp\Plugins\WSA\RosBridgeClient.dll*
* Redeploy

### Adding a new robot

#### Gazebo app

Run the command:
```bash
roslaunch file_server $NEW_ROBOT_LAUNCH_FILE.launch
```

For help regarding the launch file, look into
*~/Desktop/catkin_ws/src/file_server/launch/publish_description_turtlebot2.launch*

#### Unity app

* Go to __RosBridgeClient > Import URDF Assets__
* Check the IP Address
* Increase the timeout if necessary
* Click Open

[VM]: <https://www.virtualbox.org/wiki/Downloads>
[Unity]: <https://unity3d.com/get-unity/download/archive>
[VS]: <https://visualstudio.microsoft.com/downloads>
[Ubuntu]: <https://askubuntu.com/questions/142549/how-to-install-ubuntu-on-virtualbox>